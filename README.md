# Gdnsdfile

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/gdnsdfile`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gdnsdfile'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gdnsdfile

## Usage

#Example 

```ruby
@gdnsd = GdnsdIo::FileOperations.new({filepath: "/your/path"})
```

-This '@gdnsd' your file_path 

```ruby
@gdnsd.create_text(text_file_name)
```

-You can create text file in "{filepath: "/your/path"}" path with '@gdnsd'

```ruby
@gdnsd.write_record(@records, text_file_name)
```

-You can write your all records "{filepath: "/your/path"}" path in 'text_file_name'.txt


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/gdnsdfile. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.
