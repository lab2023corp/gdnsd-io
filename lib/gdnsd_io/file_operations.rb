module GdnsdIo
	class FileOperations
		def initialize(*args)
			options = args.last.is_a?(Hash) ? args.pop : {}
		  @config = GdnsdIo::FilePath.new options
		end


		def write_soa_record(soas, records, text_file_name)
		  tmp_file = File.new(@config.filepath + "/" + text_file_name + ".txt", "w+") unless File.exists?(text_file_name)
			soas.each do |soa|
				tmp_file.puts("$TTL" + " \t" + soa[:ttl_default].to_s + "\n" +
										"$ORIGIN" + "    "  +  text_file_name.to_s + "\n" +
										"@" + " \t " +  " IN " + " \t " + " SOA " + " \t " +  soa[:nameserver1].to_s  + " \t " + soa[:email].to_s  +  " ( " + " \n " +
										" \t\t\t " + soa[:serial_number].to_s  + " \t\t " + "; serial number YYMMDDNN" + "\n" +
										" \t\t\t " + soa[:refresh].to_s  +  " \t\t\t "  +  "; Refresh" + "\n" +
	                  " \t\t\t " + soa[:retry].to_s    +  " \t\t\t "  +  "; Retry"   + "\n" +
	                  " \t\t\t " + soa[:expire].to_s   +  " \t\t\t "  +  "; Expire"  + "\n" +
	                  " \t\t\t " + soa[:ttl_min].to_s  +  " \t\t\t "  +  "; Min TTL" + "\n" +
	                  " \t\t\t " +")"+ "\n" +
										" \t\t " +  "NS" + " \t " + soa[:nameserver1].to_s  + "\n" +
										" \t\t " +  "NS" + " \t " + soa[:nameserver2].to_s + "\n\n")
			end
			records.each do |record|
				if record[:prio].nil?
		    		tmp_file.puts(record[:name].to_s + " \t" + record[:rtype].to_s + "\t" + record[:content].to_s + "\t" + record[:ttl].to_s )
		    	else
		    		tmp_file.puts(record[:name].to_s + " \t" + record[:rtype].to_s + "\t" + record[:content].to_s + "\t" + record[:ttl].to_s + " \t" + record[:prio].to_s )
				end
			end
			tmp_file.close
		end

		def delete_text(text_file_name)
			File.delete(@config.filepath + "/" + text_file_name + ".txt")
		end
	end
end