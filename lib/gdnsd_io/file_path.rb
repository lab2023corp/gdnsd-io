module GdnsdIo
	class FilePath
		DEFAULTS = {
			filepath: ""
		}

		DEFAULTS.each_key do |attribute|
    		attr_accessor attribute
  		end

		def initialize(filepath = {})
			DEFAULTS.each do |attribute, value|
				send("#{attribute}=".to_sym, filepath.fetch(attribute, value))
			end 
		end
	end
end