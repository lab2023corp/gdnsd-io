# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gdnsd_io/version'

Gem::Specification.new do |spec|
  spec.name          = "gdnsd_io"
  spec.version       = GdnsdIo::VERSION
  spec.authors       = ["Esref Viduslu","Lab2023"]
  spec.email         = ["esref.viduslu@gmail.com"]

  spec.summary       = %q{We can use about gdnsd file operations.}
  spec.description   = %q{We have a 'path' to  save our file with .txt extension.}
  spec.homepage      = "https://bitbucket.org/lab2023corp/gdnsd-io"
  spec.licenses      = "MIT"
  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.


  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
end
